#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""TP RSA."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import string

from math import sqrt, gcd
from random import randint
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '30 October 2017'

DEFAULT_MAX = int(1e9)

def egcd(a, b):
    """Extended great common divisor.

    >>> egcd(23, 7)
    (1, -3, 10)
    >>> egcd(2, 9)
    (1, -4, 1)
    >>> egcd(0.2, 9)
    Traceback (most recent call last):
        ...
    TypeError: a must be a valid integer!

    @param a: First number.
    @type a: C{int}
    @raise a: TypeError, a must be a valid integer (C{int}).

    @param b: Second number.
    @type b: C{int}
    @raise b: TypeError, b must be a valid integer (C{int}).

    @return: gcd(a,b), u, v so au + bv = gcd(a,b)
    """
    if not isinstance(a, int):
        raise TypeError('a must be a valid integer!')
    elif not isinstance(b, int):
        raise TypeError('b must be a valid integer!')
    elif a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b//a) * y, y)

def modular_exponentiation(a, b, modulus):
    """Compute the modular exponentation of a and b.

    >>> modular_exponentiation(56, 8, 45)
    31
    >>> modular_exponentiation(688, 153, 42)
    22
    >>> modular_exponentiation(6.2, 153, 42)
    Traceback (most recent call last):
        ...
    TypeError: a must be a valid integer!
    >>> modular_exponentiation(6, 153.5, 42)
    Traceback (most recent call last):
        ...
    TypeError: b must be a valid integer!
    >>> modular_exponentiation(6, 153, 42.6)
    Traceback (most recent call last):
        ...
    TypeError: modulus must be a valid integer!

    @param a: A value.
    @type a: C{int}
    @raise a: TypeError, a must be a valid integer (C{int}).

    @param b: B value.
    @type b: C{int}
    @raise b: TypeError, b must be a valid integer (C{int}).

    @param modulus: Modulus.
    @type modulus: C{int}
    @raise modulus: TypeError, modulus must be a valid integer (C{int}).

    @return: modular exponentiation of a and b.
    """
    if not isinstance(a, int):
        raise TypeError('a must be a valid integer!')
    elif not isinstance(b, int):
        raise TypeError('b must be a valid integer!')
    elif not isinstance(modulus, int):
        raise TypeError('modulus must be a valid integer!')
    else:
        return pow(a, b, modulus)

class RSA(object):
    """RSA utils."""
    def __init__(self, min_, max_=DEFAULT_MAX):
        """Constructor of I{RSA}.

        >>> rsa = RSA(1024, 2048)
        >>> message = 'test'
        >>> payload = rsa.encrypt(message)
        >>> message == rsa.decrypt(payload)
        True

        @param self: Current instance of I{RSA}.
        @type self: C{RSA}

        @param min_: Lower bound for prime number search.
        @type min_: C{int}
        @raise min_: TypeError, min must be a valid integer (C{int}).

        @param max_: Upper bound for prime number search (defaults to system max int value).
        @type max_: C{int}
        @raise max_: TypeError, max must be a valid integer (C{int}).
        """
        if not isinstance(min_, int):
            raise TypeError('min must be a valid integer!')
        elif not isinstance(max_, int):
            raise TypeError('max must be a valid integer!')
        else:
            self.p, self.q, self.n, self.phin, self.e, self.d = self.gen_key(min_, max_)
            self.gen_alphabet()

    def encrypt(self, message):
        """Decrypt message using RSA.

        @param message: Cleartext message.
        @type message: C{str}
        @raise message: TypeError, message must be a valid C{str}.
        """
        if not isinstance(message, str):
            raise TypeError('message must be a valid string!')
        else:
            payload = []

            for char in message:
                index = self.encode_char(char)
                payload += [modular_exponentiation(index, self.e, self.n)]

            return payload


    def decrypt(self, payload):
        """Decrypt message using RSA.

        @param payload: Encrypted message.
        @type payload: C{list}
        @raise payload: TypeError, payload must be a valid C{list}.
        """
        if not isinstance(payload, list):
            raise TypeError('payload must be a valid list!')
        else:
            message = ''

            for index in payload:
                char = modular_exponentiation(index, self.d, self.n)
                message += self.decode_char(char)

            return message

    def decode_char(self, value):
        """Get index (char) from alphabet value.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.decode_char(86)
        'V'
        >>> rsa.decode_char(97)
        'a'
        >>> rsa.decode_char(277)
        Traceback (most recent call last):
            ...
        ValueError: Provided value can't be decoded!

        @param value: Value to decode.
        @type value: C{str}
        @raise value: ValueError, provided value can't be decoded!.
        """
        char = ''

        for (k, v) in self.alphabet.items():
            if v == value:
                char = k

        if char == '':
            raise ValueError('Provided value can\'t be decoded!')
        else:
            return char

    def encode_char(self, char):
        """Get index (char) from alphabet value.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.encode_char('V')
        86
        >>> rsa.encode_char('a')
        97
        >>> rsa.encode_char('É')
        Traceback (most recent call last):
            ...
        KeyError: "Provided char can't be encoded!"

        @param char: Char to encode.
        @type char: C{str}
        @raise char: KeyError, provided char can't be encoded!.
        """
        try:
            value = self.alphabet[char]
        except KeyError:
            raise KeyError('Provided char can\'t be encoded!')
        else:
            return value

    def gen_alphabet(self, charset=list(string.printable)):
        """Create alphabet for charset."""
        if not isinstance(charset, list):
            raise TypeError('charset must be a valid list!')
        else:
            self.alphabet = dict()

            for char in charset:
                candidate = ord(char)
                if not self.coprime(candidate, self.n):
                    raise ValueError(('An error occured while processing your charset! '
                                      'Please report this issue: '
                                      '{char} -> {value} (n={n})'.format(char=char,
                                                                         value=candidate,
                                                                         n=self.n)))
                else:
                    self.alphabet[char] = candidate

    @staticmethod
    def modular_inverse(number, modulus):
        """Compute the modular multiplicative inverse of an integer within modulus.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.modular_inverse(1, 13)
        1
        >>> rsa.modular_inverse(3, 26)
        9
        >>> rsa.modular_inverse(5, 89)
        18

        @param number: Number to search for modular multiplicative inverse.
        @type number: C{int}
        @raise number: TypeError, number must be a valid integer (C{int}).

        @param modulus: Modulus.
        @type modulus: C{int}
        @raise modulus: TypeError, modulus must be a valid integer (C{int}).

        @return: modular multiplicative inverse of specified number (C{int}).
        """
        g, u, _ = egcd(number, modulus)
        if g != 1:
            raise Exception('Modular inverse does not exist!')
        else:
            return u % modulus

    def gen_key(self, min_, max_=DEFAULT_MAX):
        """Return RSA keys.

        @param min_: Lower bound for prime number search.
        @type min_: C{int}
        @raise min_: TypeError, min must be a valid integer (C{int}).

        @param max_: Upper bound for prime number search (defaults to system max int value).
        @type max_: C{int}
        @raise max_: TypeError, max must be a valid integer (C{int}).

        @return: private and public key
        """
        p = self.gen_prime(min_, max_)
        q = p

        while p == q:
            q = self.gen_prime(min_, max_)

        n = p * q

        phin = (p-1) * (q-1)
        e = phin

        while not self.coprime(e, phin):
            e = randint(2, phin)

        d = self.modular_inverse(e, phin)

        return (p, q, n, phin, e, d)

    def gen_prime(self, min_, max_=DEFAULT_MAX):
        """Find a prime number between two numbers.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.is_prime(rsa.gen_prime(10, 18))
        True
        >>> rsa.gen_prime(0, 2)
        2
        >>> rsa.gen_prime(1, 1)
        Traceback (most recent call last):
            ...
        ValueError: There is no prime numbers between bounds.

        @param min_: Lower bound for prime number search.
        @type min_: C{int}
        @raise min_: TypeError, min must be a valid integer (C{int}).

        @param max_: Upper bound for prime number search (defaults to system max int value).
        @type max_: C{int}
        @raise max_: TypeError, max must be a valid integer (C{int}).

        @return: prime number (C{int}).
        """
        number = 0

        if min_ == max_ and not self.is_prime(min_):
            raise ValueError('There is no prime numbers between bounds.')
        while not self.is_prime(number):
            number = randint(min_, max_)

        return number

    @staticmethod
    def is_prime(number):
        """Check if number is prime.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.is_prime(2)
        True
        >>> rsa.is_prime(1)
        False
        >>> rsa.is_prime(13)
        True
        >>> rsa.is_prime(854)
        False
        >>> rsa.is_prime(1217)
        True
        >>> rsa.is_prime(0.7)
        Traceback (most recent call last):
            ...
        TypeError: Number must be a valid integer!

        @param number: Number to test for primality check.
        @type number: C{int}
        @raise number: TypeError, number must be a valid integer (C{int}).
        @return: number is prime (C{bool}).
        """
        result = True
        limit = int(sqrt(number) + 1)

        if not isinstance(number, int):
            raise TypeError('Number must be a valid integer!')
        elif number < 2:
            result = False
        elif number == 2:
            result = True
        elif number % 2 == 0:
            result = False
        else:
            for i in range(3, limit, 2):
                # Check only odd numbers from 3 to sqrt
                if number % i == 0:
                    result = False

        return result

    @staticmethod
    def coprime(number1, number2):
        """Check if numbers are coprime.

        >>> rsa = RSA(1024, 2048)
        >>> rsa.coprime(2, 13)
        True
        >>> rsa.coprime(1, 13)
        True
        >>> rsa.coprime(854, 1216)
        False
        >>> rsa.coprime(0.7, 18)
        Traceback (most recent call last):
            ...
        TypeError: Number #1 must be a valid integer!
        >>> rsa.coprime(7, 0.18)
        Traceback (most recent call last):
            ...
        TypeError: Number #2 must be a valid integer!

        @param number1: Number #1 to test for coprime check.
        @type number1: C{int}
        @raise number1: TypeError, number1 must be a valid integer (C{int}).

        @param number2: Number #2 to test for coprime check.
        @type number2: C{int}
        @raise number2: TypeError, number2 must be a valid integer (C{int}).

        @return: numbers are coprime (C{bool}).
        """
        result = False

        if not isinstance(number1, int):
            raise TypeError('Number #1 must be a valid integer!')
        elif not isinstance(number2, int):
            raise TypeError('Number #2 must be a valid integer!')
        elif gcd(number1, number2) == 1:
            result = True
        else:
            result = False

        return result

def main():
    """RSA main process."""
    try:
        log.bold('Your message:')
        message = input('> ')

        rsa = RSA(1024, 2048)
        payload = rsa.encrypt(message)

        log.success('Encrypted message: {}'.format(payload))

        log.info('Your public key: '
                 'n={n},'
                 ' e={e}'.format(n=rsa.n, e=rsa.e))

        log.debug('Original message: {}'.format(rsa.decrypt(payload)))
    except (KeyError, TypeError, ValueError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
